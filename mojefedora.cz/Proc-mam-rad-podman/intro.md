Ve svém (profesním) životě jsem si v poslední době velmi navyknul na používání kontejnerizační technologie Podman. Tahle technologie je kompatibilní se známějším dockerem, ale pro mě je daleko lepší, díky skvělé provázanosti na Fedoru a celý technologický stack. V tomto krátkém seriálu si ukážeme 1. K čemu se podman hodí, uděláme si jednoduchou Python Flask aplikaci 2. V dalším dílu to trochu zesložitíme, vytvoříme si složitější řešení, skládající se z na sobě závislých podů 3. Do třetice si ukážeme praktický příklad, jak můžete těžit z integrace systemd a budeme kontrolovat životní cyklus aplikací 4. Ve 4. kapitole našeho vyprávění si povíme o API k Podmanovi. Text budu psát co nejjednoduššeji, aby byl snadno přístupný i začínajícím vývojářům (nebojte se proto přeskakovat). Případně prosím o doplnění v komentářích, můj rozhled je značně omezený a stále objevuji nové věci.

## K čemu se kontejnery hodí?

Vynechám nevýhody kontejnerů, případně se k tomu vrátím později. Na Linuxu je výborné, že pokud chcete instalovat libovolnou aplikaci, budete se spoléhat na balíček, který pro vás připravil někdo v distribuci. Tento balíček obsahuje kýžený program, jež ovšem často závisí na dalších knihovnách, které může sdílet s jinými programy. Z toho vyplývá následující (víceméně):
1. Spoléháte na verzi aplikace v distribuci
2. Musíte pracovat s knihovnami, které distribuce nabízí

Kontejnery jsou postavené na funkci jádra, které umožní izolovat procesy. Pokud si chcete něco přečíst o implementaci (doporučuji) a jít trochu do hloubky, je tu odkaz na dokumentaci společnosti Red Hat a její [úvod do Linuxových kontejnerů](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/overview_of_containers_in_red_hat_systems/introduction_to_linux_containers (anglicky)). Pro naši představu můžeme velmi zjednodušeně říci, že jde o malé virtuální počítače, které mezi sebou (a s hostem) sdílí jádro operačního systému a vzájemně jsou jinak naprosto nezávislé (síť, disky, výkon, procesy ...). Tento koncept tedy přináší další výhody
1. Izolované prostředí, procesy bežící uvnitř kontejneru můžete lépe kontrolovat a zakázat jim přístup k některým funkcím
2. Jednotné prostředí, díky sestavení takovéhoto kontejneru, můžete mít jednotné prostředí, ve kterém vaše aplikace běží. Máte pod kontrolou verze knihoven, interpretů programovacích jazyků, atd. 

## Životní cyklus kontejneru, od kódu k běžící aplikaci

Ta cesta vlastně není úplně tak složitá. Pokud máte kód, který jste si předtím napsali (v následujícím příkladu použiji Python Flask) je nutné napsat předpis, který řekne, jak sestavit odpovídající kontejner. Tento předpis se jmenuje `Dockerfile` a popisuje, co vzít jako základ a co doplnit (případně uvnitř kontejneru nainstalovat, nebo spustit). No a nakonec řeknete jakýže skript/program se má pustit, označujeme jako `entrypoint` (vstupní bod). 

Se všemi těmito soubory spustíme sestavení kontejneru. Sestavení probíhá právě podle tohoto `Dockerfile`, kde interpretujete jednotlivé kroky, až se dostanete na konec, kde získáte archiv se zabaleným souborovým systémem. Mimochodem, jednotlivé kroky jsou ukládané jako vrstvy, takže po změně bude sestavení rychlejší. Balíček už můžete nahrát do repozitáře, kde se tyto kontejnery shraňují, nebo už spustit. Pokud budete kontejner spouštět, tak budete specifikovat, jak přesně bude přistupovat do sítě, jestli s ním chcete sdílet některé adresáře, nebo jestli rovnou může otevřít port na vašem systému (vlastně NAT)

## První praktický příklad
Budeme pracovat s `podman`em, který rychle nainstalujete na Fedoru/CentOS, případně se [podívejte](https://podman.io/getting-started/installation), jak ho dostat do vaší oblíbené distribuce. 
```dnf install podman```

Podman narozdíl od Dockeru umožňuje běžet v uživatelském sezení a není potřeba mít přístup na socket. následující příklad spustí nginx (webový server) na lokálním portu 8000. Všimněte si, že v systému máte předdefinované nějaké repozitáře, které už obsahují kontejnery, které zabalil někdo jiný (tak mu musíte věřit, že zabalil, co měl, a ne nic navíc). Následující příklad také obsahuje přepínač `-p`, co říká jak mapovat porty kontejneru (o tom později). 
```podman run -p 8000:80 nginx```
![Náhled na stránku v prohlížeči](Screenshot_from_2021-02-13_21-05-00.png)

Otestovat můžete v prohlížeči, nebo použitím příkazu `curl localhost:8000`. Pak asi budete chtít tento experiment ukončit. To by mělo být jednoduché, že v okně s běžíčím kontejnerem zmáčknete klávesovou zkratku `Ctrl+C`. Trochu lepší přístup je vypnout tento kontejner z hosta. Tady rovnou zjistíme, že kontejnery dostávají nějaké unikátní ID (hash). Pamatovat si ovšem sha256 je prakticky nemožné, proto kontejnery mají i jména (nedáte-li ho jim vy, dostanou náhodné). Zjistíme tedy jaké jméno kontejner má a tento kontejner "zabijeme" a smažeme. 
![Podman ps prozradí, jaké je ID kontejneru, který chceme vypnout](Screenshot_from_2021-02-13_21-07-56.png)
```
podman ps
podman kill <jméno/id kontejneru>
podman rm <jméno/id kontejneru>
```
## Vlastní aplikace
Vytvoříme banální aplikaci napsanou ve Pythonu s frameworkem Flask. Doporučuji vytvořit si nějaký adresář, třeba `HelloWorld`. Nainstalujte si Flask `pip install --user flask` abychom mohli aplikaci lokálně otestovat (lokálně doporučuji používat [virtuální prostředí](https://docs.python.org/3/library/venv.html)). Teď už je triviální vytvořit soubor `app.py` obsahující aplikaci, co na portu 8000 spustí webový server, co pozdraví a řekne jméno systému, kde běží:
```
from flask import Flask
import os

app = Flask("HelloWorld")

@app.route("/")
def root():
    return "<h1>Ahoj z kontejneru</h1><p>{}</p>".format(os.environ.get("HOSTNAME", "Neznámý systém"))

app.run()
```
Aplikaci si samozřejmě otestujte (`python app.py`) a podívejte se v prohlížeči na adresu [localhost:5000](localhost:5000), kde by snad měla běžet. Pokud je vše v pořádku, přejdeme na vytvoření `Dockerfile`, kde vytvoříme kontejner postavený na Fedoře. Tedy zapišme do souboru `Dockerfile` následující (řádky s `#` jsou komentáře, klidně je vynechte):
```
# Na jakém obraze stavíme
FROM fedora:latest
# Instaluji python
RUN dnf install -y python python3-pip
# Instaluji flask
RUN pip install flask
# Kopíruji dovnitř soubor s aplikací
ADD app.py /app.py
# Definuji vstupní bod
ENTRYPOINT python app.py
```

Sestavení aplikace (za předpokladu, že jsme v umístění se soubory `app.py` a `Dockerfile`) už proběhne snadno - `podman build -t  muj-kontejner .`. Teď to chvilku potrvá a můžete vidět, jak se postupně zpracovávají jednotlivé kroky a jak se mezistavy ukládají (vidíte otisky jednotlivých vrstev). 

Připravený kontejner spustíme `podman run --rm -p 5000:5000 muj-kontejner`. Přidávám přepínač `-p 5000:5000` (lze také zapsat jako `-p 5000`), pro předání portu 5000 ze spouštěcího počítače na kontejner a přepínač `--rm` pro okamžité smazání kontejneru při jeho ukončení. Nezapomeňte ověřit, že se všechno povedlo pomocí prohlížeče, nebo příkazu `curl localhost:5000`. Všimněte si, že aplikace vrací jméno systému, na kterém běží a ten je v tomto případě jiný (a při opakovaném spuštění se změní). Pokud máte problémy s ukončením kontejneru, podívejte se o pár řádek výš. 
![A tady už je pozdrav z kontejneru](Screenshot_from_2021-02-13_21-19-12.png)

## Závěr
Dnes jsme si nastínili velmi triviální úvod do kontejnerů a jejich implementaci (použití) pomocí nástroje [podman](podman.io). Pokud Vám dnešní díl nic nepřinesl, příště to snad bude už lepší, podíváme se, jak kontejnery seskupovat, aby mohla aplikace přistupovat k databázi a stále šlo o plně izolované nasazení. Máte-li nějakou otázku, nebo jste narazili na nepřesnost, pište do komentářů, rád se přiučím, nebo odpovím. Ideální prostor na jakékoli otázky je naše [fórum](http://forum.mojefedora.cz).

