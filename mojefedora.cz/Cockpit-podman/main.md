Cockpit - Webové grafické rozhraní pro servery
Cockpit je určet pro nové uživatele Linuxu, zkušenější, co chtějí rychlé přehledné GUI, experty, kteří používají jiné nástroje, ale chtějí mít přehled nad jednotlivými systémy. 

Cockpit je modulární aplikace v jejímž základu běží webový server, který přes websocket komunikuje s démonem běžícím na vašem počítači. Pokud jste neudělali minimální instalaci, pravděpodobně už ho můžete spustit i u Vás, případně si ho doinstalujte pomocí `dnf`. 

```
systemctl start cockpit.socket
```

Na počítači poběží na portu `9090`, takže při místní instalaci bude mít adresu `https://localhost:9090`. Při instalaci na vzdálené stroje budete pravděpodobně řešit SSL certifikáty, buďto použijete `self-signed`, nebo podepsané nějakou autoritou, třeba `Let's Encrypt` (Fedora Linux obsahuje balíček `certbot`). 

Díky cocpitu se můžete prostřednictvím webového prohlížeče můžete připojovat na místní i vzdálené počítače a prohlížet jejich stav, logy a další. Ostatně podívejte se jaké všechny moduly najdete ve standardním repozitáři (instalace je možná i z webového rozhraní). 
```
cockpit-389-ds.noarch : Cockpit UI Plugin for configuring and administering the 389 Directory Server
cockpit-bridge.x86_64 : Cockpit bridge server-side component
cockpit-composer.noarch : Composer GUI for use with Cockpit
cockpit-doc.noarch : Cockpit deployment and developer guide
cockpit-file-sharing.noarch : Cockpit user interface for managing SMB and NFS file sharing.
cockpit-kdump.noarch : Cockpit user interface for kernel crash dumping
cockpit-machines.noarch : Cockpit user interface for virtual machines
cockpit-navigator.noarch : A File System Browser for Cockpit
cockpit-networkmanager.noarch : Cockpit user interface for networking, using NetworkManager
cockpit-ostree.noarch : Cockpit user interface for rpm-ostree
cockpit-packagekit.noarch : Cockpit user interface for packages
cockpit-pcp.x86_64 : Cockpit PCP integration
cockpit-podman.noarch : Cockpit component for Podman containers
cockpit-selinux.noarch : Cockpit SELinux package
cockpit-session-recording.noarch : Cockpit Session Recording
cockpit-sosreport.noarch : Cockpit user interface for diagnostic reports
cockpit-storaged.noarch : Cockpit user interface for storage, using udisks
cockpit-system.noarch : Cockpit admin interface package for configuring and troubleshooting a system
cockpit-tests.x86_64 : Tests for Cockpit
cockpit-ws.x86_64 : Cockpit Web Service
subscription-manager-cockpit.noarch : Subscription Manager Cockpit UI
```

Cockpit nedávno dostal i desktopového klienta, který umí se připojit přes SSH na server, který cockpit obsahuje, ale nemusí mít nainstalovanou webovou verzi (spojí se se vzdáleným websocketovým sezením). 

```
flatpak install flathub org.cockpit_project.CockpitClient
```

V cockpitu se za poslední dobu udělalo velké množství práce. Podporuje virtuální počítače, podman kontejnery, selinux a zdá se, že i instalátor Anaconda (bude)[https://mojefedora.cz/instalator-anaconda-bude-mit-webove-rozhrani/] na cockpitu postavený.  aktualizace a třeba i terminál. Podívejte se do naší galerie na to jak cockpit vypadá, protože příště si už vyrobíme do cockpitu vlastní modul. 