## Wireguard

S příchodem jádra 5.6 do Fedory si můžeme užít novinku, která potěší mnohé (včetně mě), kteří mají již po krk složitě nastavovaných VPN a hledají jednoduše spravovatelnou VPN. V dnešním článku bychom si tak sestavili jednoduchý komunikační tunel.

### Instalace
Budeme potřebovat nějaké nástroje, abychom mohli pracovat se speciálním síťovým rozhraním. Jako server mi poslouží čerstvá instalace Fedora server 32 do virtuálního počítače a klienta udělám ze svého počítače. Hovořit o klientovi a serveru nedává moc smysl, protože spojení jsou p2p (peer to peer), ale používá se to pro popis "architektury"

`dnf install wireguard-tools`
Vytvoříme si adresář pro uchovávání konfigurace wireguard. 
`sudo mkdir /etc/wireguard`

### Generování klíčů
V předchozím kroku jsme nainstalovali nástroje pro ovládání wireguard, teď jdeme připravit klíče pro komunikaci (wireguard funguje na principu asymetrického šifrování). Spusťte na obou uzlech.
```
# Složka s konfigurací
cd /etc/wireguard/
# Nově vytvořené soubory nesmí být čitelné nikomu kromě vlastníka
umask 077
# Vygeneruji si soukromý klíč, jeho výstup uložím do souboru a výstup rovnou předám pro generování veřejného klíče
wg genkey | tee privkey | wg pubkey > pubkey
```

... a je vygenerováno. Ověřte si, že klíče jsou chráněné. Veřejný klíč je kdykoliv možné dopočítat a WG ho používá pro identifikaci, takže ho uvidíte ve výstupech z některých příkazů
```
# Ověření práv na souborech
ls -l *key
-rw-------. 1 root root 45 May 15 11:00 privkey
-rw-------. 1 root root 45 May 15 11:00 pubkey
```

### Konfigurace rozhraní
Jakmile máme klíče připravené, půjdeme nakonfigurovat rozhraní jednotlivých strojů. Připomínám zde, že wireguard je "by design" p2p a nenechte se zdát, že konfigurujeme klient->server. Tato konfigurace pouze určuje, že spojení mezi oběma uzly navazuje pouze jeden (typické nasazení VPN). 

#### "Server" 
Tento stroj budeme uvažovat jako server, protože do budoucna může být konfigurován tak, že může mezi jednotlivými uzly směřovat komunikaci. Pro vytvoření konfigurace budu potřebovat

1. Soukromý klíč tohoto serveru
1. Veřejný klíč vzdáleného bodu
1. Připravit si zamýšlený IP rozsah
1. Připravený adresář `/etc/wireguard` z kroku 1

Do souboru `/etc/wireguard/wg0.conf` si doplníme následující konfiguraci. Soubory `/etc/wireguard/*.conf` jsou soubory, které definují vlastnosti jednotlivých rozhraní, konfigurujeme tak rozhraní `wg0`. 
```
[Interface]
Address = 192.168.136.1/24
SaveConfig = true
ListenPort = 51820
PrivateKey = <Server.Privatekey>

[Peer]
PublicKey = <Peer.PublicKey>
AllowedIPs = 192.168.136.0/24
```

Nejprve v sekci **Interface** nastavíme IP adresu rozhraní, port na kterém služba naslouchá a soukromý klíč hosta. **SaveConfig** říká, že pokaždé když rozhraní *vypneme* má dojít k uložení konfigurace. 

**Peer** definuje veřejný klíč očekávaného uzlu a IP adresy, kterými budete komunikovat. Nenechte se (jako já) zmást tím, že při běhu Wireguardu si sem uloží port (položka **Endpoint**) na kterém naposledy komunikovali, aby pak mohl komunikaci později otevřít ze své strany i tento "Server". 

Zabezpečte si konfiguraci a její práva `chmod 600 /etc/wireguard/wg0.conf` a přiveďte rozhraní k životu `wg-quick up wg0`. 

Otevřete si port na firewallu, nastavte automatické spouštění služby (rozhraní) při startu počítače a podívejte se, jak to běží. :) 
```
sudo systemctl enable wg-quick@wg0
firewall-cmd --add-port 51820/udp --permanent
firewall-cmd --reload
wg
# Očekávaný výstup
# interface: wg0
#   public key: <Server.PublicKey>
#   private key: (hidden)
#   listening port: 51820

```

#### Klient
Připravím si veřejný klíč serveru, soukromý klíč tohoto hosta, a IP adresu z požadovaného rozsahu a informace doplním do `/etc/wireguard/wg0.conf`. 
```
[Interface]
Address = 192.168.136.2/24
PrivateKey = <Client.PrivateKey>

[Peer]
PublicKey = <Server.PublickKey>
Endpoint = <IP adresa serveru>:51820
AllowedIPs = 192.168.136.0/24
```

Nenastavuji port, protože spojení bude navazovat tento uzel a informace předá tomu druhému sám. Akorát musí vědět kam se připojí, s jakou IP adresou a v jaké síti budou spolu komunikovat. 

A je to! Rozhraní spustím pomocí `wg-quick up wg0`. 

#### Co říci závěrem?

Doufám, že vás, stejně jako mě, wireguard zaujal a budu moc rád, pokud se o své postřehy podělíte v komentářích. Případné otázky se pokusím zodpovědět, ačkoliv moje znalosti nejdou o moc víc nad rámec tohoto článku. 