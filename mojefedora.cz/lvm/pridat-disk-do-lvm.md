## Rozšíření LVM
Mám na svojí instalaci (ještě před BTRFS) jeden NTFS oddíl. Ovšem prakticky ho nepoužívám, takže mi tu jenom zabírá místo. Nechci se ho zbavit úplně, ale rád bych ho omezil co se dá. Jsem příliš líný bootovat do čehokoliv, co na něm běží a tak veškeré úpravy uděláme z Fedory. 

## Zálohy
Nezapomeňte, vždy když pracujete s diskem udělat si zálohy! Jak snadné je přijít o data a nemít odkud je získat! Ačkoliv na nástrojích pro práci s NTFS oddíly se neustále pracuje, jsou tu stále rizika a nemusíte už do původní instalace Windows nikdy nabootovat. 
## Vypneme Windows pořádně
Windows se nevypíná, ale běžně hibernuje, takže potřeba je vypnout [pořádně](https://www.howtogeek.com/349114/shutting-down-doesnt-fully-shut-down-windows-10-but-restarting-it-does/). Případně lze hibernační soubor smazat při připojování přes ntfs-3g (pak ho zase odpojte).

```
sudo mount -t ntfs-3g -o remove_hiberfile /dev/sda5 /mnt/win
```

*Všechny úpravy by mělo být možné udělat přes blivet-gui, který si můžete nainstalovat ve Fedoře, jeho autorem je Vojtěch Trefný a tak vám třeba na našem [Telegramu]() třeba i pomůže. Mě to při přípravě článku nenapadlo, tak jsem většinu dělal v příkazové řádce, jak jsem zvyklý.*

## Sebereme NTFS místo
Pustím si GNOME disks a zmenším v GUI oddíl s NTFS. Nechám Windows oddílu trochu místa, kdybych opravdu náhodou do něj chtěl nabootovat. Operace trvá nějakou chvíli i na SSD, čas na kávu a chutný dezert z blízkého pekařství.
## Struktura LVM
Pokud nevíte (nebo si stejně jako já nejste jisti) s tím, z čeho se skládá LVM, podívejte se do [dokumentace](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/lvm_definition).

<!-- Jaká je česká terminologie? -->
Fyzické disky/oddíly (Physical volume) se seskupují do Skupin oddílů (Volume Group), které "nabízí" systému virtuální úložiště přes "Logické svazky" (Logical Volume). 

Jak získat přehled? 
```
pvs # Rychlý přehled Physical volumes
pvdisplay # *P*hysical *V*olumes *display*
vgs # Rychlý přehled Volume groups
vgdisplay # *V*olume *G*roups *display*
lvs # Rychlý přehled Logical Volumes
lvdisplay # *L*ogical *V*olume *display*
```

V galerii vidíte výstup všech příkazů v mém počítači, byť u `Logical volumes` zkrácený.

## Rozšiřujeme `Logical Volume`
(To zmenšování NTFS oddílů opravdu trvá dlouho.). Abychom mohli zvětšit `Logical Volume` je potřeba nejprve zvětšit `Volume Group`, kterou můžeme rozšířit pouze přidáním `Physical Volume`. To můžeme vytvořit z místa, které nám uvolnil NTFS oddíl. 

Abychom mohli s diskem pracovat, udělám pomocí GNOME disks nový oddíl. Není nutné volit konkrétní formátování, protože na něm vůbec nezáleží, protože disk nebudeme používat přímo. Fanoušci terminálu použijí (`fdisk /dev/sdX`). 

Operace jsou následující (doplnil jsem je o komentáře)
```
# Vytvořím Physical Volume a nevadí, že mi to smaže zrovna vytvořený ext 4 oddíl. 
[root@mojefedora ~]# pvcreate NTLiberated /dev/sdb8 
  No device found for NTLiberated.
WARNING: ext4 signature detected on /dev/sdb8 at offset 1080. Wipe it? [y/n]: Y
  Wiping ext4 signature on /dev/sdb8.
  Physical volume "/dev/sdb8" successfully created.
# Nový Physical Volume vidím, ale bez Volume Group
[root@mojefedora ~]# pvs
  PV         VG                    Fmt  Attr PSize    PFree  
  /dev/sda4  Linux                 lvm2 a--  <443,23g      0 
  /dev/sdb3  fedora_localhost-live lvm2 a--   429,92g   8,00m
  /dev/sdb8                        lvm2 ---   114,34g 114,34g
# Rozšířím odpovídající Volume Group
[root@mojefedora ~]# vgextend fedora_localhost-live /dev/sdb8
  Volume group "fedora_localhost-live" successfully extended
# Výpis Volume Groups mi jasně ukazuje, že přibylo místo
[root@mojefedora ~]# vgs
  VG                    #PV #LV #SN Attr   VSize    VFree  
  Linux                   1   2   0 wz--n- <443,23g      0 
  fedora_localhost-live   2   3   0 wz--n- <544,27g 114,35g
# Najdu si cestu k Logical volume, které chci zvětšit (hledám LV path)
[root@mojefedora ~]# lvdisplay 
...
  --- Logical volume ---
  LV Path                /dev/fedora_localhost-live/home
  LV Name                home
...
# Přidám mu 100% toho, co bylo volné
# Při revizi článku mi bylo doporučeno, že lvextend umí parametr -r/--resize, který rozšíření souborového systému udělá rovnou.
[root@mojefedora ~]# lvextend -l +100%FREE /dev/fedora_localhost-live/home
  Size of logical volume fedora_localhost-live/home changed from <348,14 GiB (89123 extents) to <462,49 GiB (118397 extents).
  Logical volume fedora_localhost-live/home successfully resized.
# Resize2fs umí rozšířit oddíl, i když je připojený (to zní nebezpečně, reálná rizika neznám, ale opět je dobré mít zálohu)
[root@mojefedora ~]# resize2fs /dev/fedora_localhost-live/home 
resize2fs 1.46.3 (27-Jul-2021)
Filesystem at /dev/fedora_localhost-live/home is mounted on /home; on-line resizing required
old_desc_blocks = 44, new_desc_blocks = 58
The filesystem on /dev/fedora_localhost-live/home is now 121238528 (4k) blocks long.
# Nicméně můj /home oddíl už má spoustu nového místa
[root@mojefedora ~]# df -h | grep home
/dev/mapper/fedora_localhost--live-home  455G  314G  120G  73% /home
```

Dnes jsme si ukázali jak rozšířit LVM. Nové instalace (od Fedora 33) už mají jako výchozí BTRFS, kde by tato operace měla být daleko jednodušší (vlastně jenom `btrfs device add`). O Btrfs na release party Fedory 33 [povídal](https://youtu.be/6hVysgjokhc?t=2327) Vojtěch Trefný a [něco](https://www.suse.com/support/kb/doc/?id=000018798) určitě najdete na internetu. 



