GNOME Shell umožňuje instalovat rozšíření, která přidávají, nebo mění chování oproti standardu. Dříve se rozšíření instalovala přes webové rozhraní na extensions.gnome.org, přes Správce software, pak Rozšíření a Matt Jakeman vydal svého Správce rozšíření. Ten se teď dostal do verze 2.x.x. 

Nová verze verze přinesla zásadní přepsání do moderní `libadwaita` (sada widgetů podle GNOME HIG) a GTK 4. Takže celá aplikace je responzivní a neměl by být problém jí provozovat na menších obrazovkách. Další výhody nové knihovny je třeba respektování barevného schématu (o tmavém schématu jsme [psali](https://mojefedora.cz/tmavy-rezim-v-gnome-42/)). Lidadwaita podporu akcentace (barevného tónu) používá třeba Elementary, nebo Ubuntu a jde o alternativu dřívějším "divokým" úpravám aplikací (viz iniciativa "[Prosím neupravujte vzhled našich aplikací](https://stopthemingmy.app/)". 

Z hlediska funkčnosti přináší významnou změnu v možnosti hledat a instalovat rozšíření přímo v aplikaci. Při hledání nově rovnou vidíte jestli vaše verze Shellu je oficiálně vývojářem podporovaná. Vzpomeňme, že Shell 40 přinesl zásadní rozdíl v rozvržení a autoři museli přepisovat svá rozšíření. 

Každé rozšíření je možné rozkliknout a podívat se na popis, náhled (přidáno ve verzi 2.0.1) nebo na seznam kompatibilních verzí GNOME Shell a tam taky najdete odkaz na webovou stránku rozšíření (na extensions.gnome.org)

Instalujte už dnes z [flathubu](https://flathub.org/apps/details/com.mattjakeman.ExtensionManager), nebo se podívejte na [github](https://github.com/mjakeman/extension-manager), kde se vývoj odehrává. 

