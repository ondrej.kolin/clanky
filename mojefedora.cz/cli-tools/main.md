# Užitečné nástroje pro CLI
Ve Fedoře najdete velkou spoustu nástrojů hned po instalaci. Ovšem některé jsou tu už s námi delší dobu a i přes neoddiskutovatelnou kvalitu a _užitnost_ se může zdát, že už jsou za zenitem. Podívejme se jak si zlepšit život pár užitečnými nástroji pro příkazovou řádku. 

## Bat - Cat s výrazněním syntaxe a podporou gitu
Účelem příkazu `cat` je spojování (zobrazování) souborů. Ovšem bat vám navíc ukáže barevně syntaxi, nebo změny v gitu. Pokud se výstup nevejde na obrazovku, máte možnost procházet text podobně jako třeba v `less` (procházení vypnete pomocí Q). 

Instalovat můžete jednoduše pomocí `dnf install bat`. 

TIP: Nastavte si bat jako `$PAGER` pomocí `export PAGER=bat` ve vašem `.bashrc` a nebudete litovat.

![Zvýraznění syntaxe](images/bat1.png)
![Zobrazení --plain](images/bat2.png)
![Podpora gitu](images/bat3.png)

## Diff so fancy - Barevný diff
Někdy je výchozí výstup z `diff` trochu mdlý a nemusí být zřejmé, kde je jaký rozdíl. Na tomto místě nastupuje [`diff-so-fancy`](https://github.com/so-fancy/diff-so-fancy). Výstup zpracuje, takže okamžitě vidíte, kde se co změnilo. 

Tenhle nástroj nenajdete ve standardním repozitáři, ale můžete si ho nainstalovat z npm `npm i diff-so-fancy`, nebo jejich git repozitáře. Jenom nezapomeňte správně nastavit proměnnou `$PATH`, ať ho spustíte odkudkoliv.

![Rozdíl proti standardnímu diffu](images/diff.png)

Nastavte si ho jako výchozí `diff` pro git.

```
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global interactive.diffFilter "diff-so-fancy --patch"
```

## FX - Čtení jsonů nikdy nebylo lehčí
FX je skvělý nástroj na práci s JSONem, umožnuje filtrování, procházení, apod. v jednoduchém textovém rozhraní přímo ve vaší konzoli. Má několik funkcí

1. Procházení
  Jednoduše můžete procházet JSON objekt kurzorovými šipkami. Dolů a nahoru pro procházení a doleva, doprava použijete, pokud chcete nějaký objekt nechat expandovat, nebo opět zavřít. P.S.: A to i myší!

2. Navigace v objektu
Procházet ale objekt ručně může být náročné, můžete pomocí klasické notace procházet jednotlivé atributy. Zmáčkněte `.` (tečku) a `fx` nabídne seznam atributů daného objektu, vybíráte kurzorovými šipkami. 
3. Filtrování a manipulace s daty pomocí Javascriptu
FX můžete použít i neinteraktivně přímo na příkazové řádce, jenom mu pošlete obsah na standardní vstup. Rovnou si ukážeme, že umí vyfiltrovat všechna lichá čísla pomocí JS funkce `.filter`
```
echo "[1, 3, 5, 2, 4, 6]" | fx '.filter(i => i % 2)'
[
  1,
  3,
  5
]
```

Všimněte si, že fx vstup namapuje na proměnnou `this`, což je vidět v následujícím příkladě, kde rozšíříme objekt pomocí 
[spread](https://en.wikipedia.org/wiki/JavaScript_syntax#cite_ref-13) operátoru.
```
echo '{"foo":"bar"}' | fx '{...this, url:"https://mojefedora.cz"}'
{
  "foo": "bar",
  "url": "https://mojefedora.cz"
}
```

Na Medium vyšel [článek](https://medium.com/@antonmedv/discover-how-to-use-fx-effectively-668845d2a4ea) (ENG), jak FX používát naplno. Případně se podívejte na [video návod (ENG)](https://www.youtube.com/watch?v=ktfeRxKog98). 

# FZF - Fuzzy Search

FZF je nástroj pro rychlé vyhledávání. Je napsaný v Go a najdete ho v repozitářích (instalace `dnf install fzf`). Vyhledávat (filtrovat) můžete cokoliv, seznam balíčků, soubory, ...

![Filtrujeme balíčky dnf search python, které obsahují django a bash](images/fzf1.png)

Jednotlivé termíny, podle čeho hledat oddělujete mezerou. Můžete také použít různé operátory pro zpřesnění, nebo vyřazení některých výsledků. Co jste "trefili" je barevně zvýrazněno. 

V tomto příkladu hledám slovo "Screenshot" (všimněte si _překlepu_, fzf hledá přibližnou shodu). Vyhledávání zpřesňuji pomocí `'2020`, kde operátor `'` určuje přesnou shodu. Jako poslední jsem zapsal negaci hledání přesné shody na konci `!0.png$` (vyřaď všechno, co končí přesně na `0.png`)

![Filtrujeme screenshoty](images/fzf2.png). 

No a když filtrujete, není problém zobrazovat náhled souborů. Třeba s použitím `bat` z úvodu. Kód vypadá takhle a klíčový je přepínač `--preview`, syntax je podobná find (přepínače u bat můžete vynechat. Náledující příklad ukazuje funkčnost náhledu (`fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'`)

![`fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'`](images/fzf3.png)

Pokud jste našli, co jste hledali, vyberte to kurzorovými šipkami (ovládat jde i myší), stikněte enter a `fzf` to vypíše na standardní výstup, takže podobně jako `fx` ho můžete použít ve skriptech.  

| Token     | Typ shody                 | Popis                          |
| --------- | -------------------------- | ------------------------------------ |
| `sbtrkt`  | fuzzy-match                | Něco, co obsahuje (nebo podobné) `sbtrkt`            |
| `'wild`   | exact-match (quoted)       | Obsahuje text  `wild`            |
| `^music`  | prefix-exact-match         | Něco, co začíná na  `music`        |
| `.mp3$`   | suffix-exact-match         | Vše, co končí na  `.mp3`           |
| `!fire`   | inverse-exact-match        | Něco, co neobsahuhe `fire`     |
| `!^music` | inverse-prefix-exact-match | Prvky, které nezačínají na music `music` |
| `!.mp3$`  | inverse-suffix-exact-match | A ty, které nekončí na `.mp3`    |

Informace můžete najít v `man fzf`, nebo na [githubu](https://github.com/junegunn/fzf#usage) projektu. 

Dnes jsme si představili 5 nástrojů, které můžete začít ihned používat a zjednodušší život každému uživateli příkazové řádky. Který je váš fanoušek? Máte nějakého dalšího, podělte se v komentářích. 

https://twitter.com/amilajack/status/1479328649820000256?t=Cg28AYIkPuki-gOLvdDDjA