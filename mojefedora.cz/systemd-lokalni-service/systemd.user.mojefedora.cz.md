# Systemd pro uživatele
Pokud se setkáváte s příkazy z kolekce systemd, typicky jde o administrátorské úlohy, jako zapnout webový server, vypnout databázi, restartovat úlohy, či případně pokročilejší si kontrolují kontejnery v podmanovi, apod. Napadlo mě ale, že uživatelské sezení systemd služby také používá a že bych si přes to mohl pouštět svoje lokální testovací prostředí při vývoji webů

## Současný stav
 Aplikace, kterou vytvářím se skládá z backendu (v mém případě padla volba na Python + Django) a na frontend, který je psaný v Reactu. Moje běžné workflow tak bylo pustit backend a vedle něho v terminálu frontend. Ale znamená to, vlézt do správné složky, aktivovat virtuální prostředí Pythonu a spustit vývojový server, který má tu skvělou vlastnost, že při změnách v souborech se znovu načte. Pak ale samozřejmě, otevřu druhé okno a pustím právě tu aplikaci přes  `npm start`.

To chceme změnit. Jedním příkazem můžu se systemd pustit obě _nezávislé_ komponenty na jednou a případně je dokonce můžu spouštět při startu sezení mého uživatele.
![Tilix](file:///home/ondrej/Pictures/tilix-backend-frontend.png)

## Kam s nimi?
Mně se líbí umístění v `~/.config/systemd/user/`, kam můžu své vlastní definice služeb nahrávat. Pravděpodobně tato složka u Vás neexistuje, tak si ji založte. 

```mkdir -p ~/.config/systemd/user/```

A vlastně je to celkem jednoduché. Vytvořím si v této složce soubor `backend.service` s následujícím obsahem:
```
[Unit]
Description=Backend server

[Service]
ExecStart=/home/ondrej/backend/venv/bin/python /home/ondrej-backend/manage.py runserver

[Install]
WantedBy=default.target
```

Pro frontend by to bylo taky velmi jednoduché nebýt toho, že celý server se ukončuje při nalezení EOF na konci standardního vstupu. Samozřejmě v případě neinteraktivního sezení to je hned. Dostal jsem nápad, že bych mu mohl předat jako standardní vstup nějaký nekončící soubor (jak hloupý nápad). Stále opakované čtení z `/dev/zero` vedlo ke skokovému nárůstu využití CPU. Naštěstí jsem poté objevil proměnnou, která zabraňuje tomuto nežádoucímu chování a toto zbytečné čtení ze souboru jsem mohl smazat.

```
[Unit]
Description=Project

[Service]
ExecStart=/usr/bin/npm run start --prefix /home/ondrej/project
Environment=CI=true

[Install]
WantedBy=default.target
```

Nezapomeňte systemd nechat znovunačíst konfiguraci s `systemctl --user daemon-reload`.

## Co z toho mám?
Teď můžu ovládat svoje projekty vesele přes `systemctl --user` a je velmi snadné je startovat při otevření uživatelského sezení. Zároveň využiji `journal` a mám zadarmo kvalitní log s daty a historií. Prostě paráda. 

```
# Spustit backend/frontend
systemctl --user start backend
systemctl --user start frontend
# Podívat se na záznamy toho backendu (-u jednotka a -f chci číst všechny nově příchozí záznamy)
journalctl --user -uf backend
# Povolit spouštění obou služeb při přihlášení
systemctl --user enable backend frontend
```