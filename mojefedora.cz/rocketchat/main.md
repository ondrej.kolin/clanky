## Rocket.chat
Rocket.chat je *slack-like* platforma, která umožňuje komunikaci v reálném čase, včetně členění na místnosti, skupiny a spoustu dalších užitečných funkcí (sdílení souborů, videohovory, apod.). Funkčnost je rozšiřitelná pomocí pluginů, aplikace jako taková je napsaná v Node.js. Dnes bychom si ukázali, jak Rocket.chat nainstalovat do Fedory. Zároveň po cestě vytvoříme systemd unitu, reverse proxy a budeme řešit selinux.

### Instalace
Rocket.chat je node.js aplikace, která data ukládá do MongoDB. Jako proxy nainstalujeme nginx, která bude směřovat provoz na aplikační port pro Rocket.chat.

## MongoDB
Jde o známou a rozšířenou NoSQL databázi, bohužel kvůli změně licenční politiky byla ze standardních repozitářů [https://fedoramagazine.org/how-to-get-mongodb-server-on-fedora/](článek na Fedora Magazine [ENG]). Tedy je nutné přidat externí repozitář, databázi nainstalovat a spustit.

```bash
    # pustte si root shell pomoci "sudo -i"
	# přidání repozitáře
cat > /etc/yum.repos.d/mongodb.repo <<EOF
[mongodb-upstream]
name=MongoDB Upstream Repository
baseurl=https://repo.mongodb.org/yum/redhat/8Server/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOF
	# instalace a spuštění MongoDB
dnf install mongodb-org
systemctl enable --now mongodb-org
```

## Softwarové závislosti 
Instalace balíčků a NPM modulů. GraphicsMagick je sada nástrojů pro manipulaci s grafickými daty, umožňuje tak obrázky zmenšovat, ořezávat, převádět bez nutnosti použití grafické nádstavby.
```bash
	# poustime root shell "sudo -i"
dnf install -y gcc-c++ make mongodb-org nodejs GraphicsMagick
npm install -g inherits n
```

## Stažení a konfigurace Rocket.chat
Pomocí nástroje `curl` stáhneme poslední vydání a uložíme do dočasného adresáře `tmp`. Cesty je samozřejmě možné měnit. Rozbalení a přípravu je možné udělat přímo v `tmp`. Poté je potřeba doinstalovat závislosti balíčkovacího systému `npm`. 
```bash
curl -L https://releases.rocket.chat/latest/download -o /tmp/rocket.chat.tgz
mkdir /usr/src/rocketchat
tar -xzf /tmp/rocket.chat.tgz -C /usr/src/rocketchat
cd /usr/src/rocketchat/bundle/programs/server && npm install
```

## Příprava na spouštění v systému
Vytvoření uživatele, pod kterým bude Rocket.Při vytváření také nechceme uživateli vytvářet domovský adresář a uživatele uzamkneme, protože nechceme povolit přihlašování uživatele. Aby uživatel mohl spravovat aplikaci, musí mít práva na souborech aplikace.  

```bash
useradd -M rocketchat && sudo usermod -L rocketchat
chown -R rocketchat:rocketchat /opt/Rocket.Chat
```

## Systemd unit
Pro spouštění aplikace jako součást systému vytvoříme následujícím příkazem. Aplikaci chceme spouštět po spuštění sítě a nginx proxy. Aplikace konfigurační data čerpá z proměnných prostředí. Pokud chcete provozovat mongo DB na jiném stroji, zde můžete konfiguraci upravit.
```bash
tee  /lib/systemd/system/rocketchat.service<< EOF
[Unit]
Description=The Rocket.Chat server
After=network.target remote-fs.target nss-lookup.target nginx.target mongod.target
[Service]
ExecStart=node /opt/Rocket.Chat/main.js
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=rocketchat
User=rocketchat
Environment=MONGO_URL=mongodb://localhost:27017/rocketchat?replicaSet=rs01 MONGO_OPLOG_URL=mongodb://localhost:27017/local?replicaSet=rs01 ROOT_URL=http://localhost:3000/ PORT=3000
[Install]
WantedBy=multi-user.target
EOF
```

Načteme novou konfiguraci pro systemd, a spustíme server. Samozřejmě, že ho odteď budeme pouštět při každém startu. Můžete se podívat, jak se pouští do journalu. Odejdete stiskem `Ctrl + C`.

```
systemctl daemon-reload
systemctl enable --now rocketchat
journal -fu rocketchat
```

## Instalujeme a nastavujeme proxy
Je dnes běžné, že aplikace běží za některou proxy a nepřistupuje se na ní přímo. Oficiální dokumentace ukazuje konfiguraci pro nginx, který je potřeba nejprve nainstalovat, vytvořit konfiguraci a později spustit.

```bash
sudo dnf install -y nginx
# Konfigurace nginx
sudo tee /etc/nginx/conf.d/default.conf <<EOF
# Upstreams
upstream backend {
    server 127.0.0.1:3000;
}

# HTTP Server
server {
    listen 80;

    # You can increase the limit if your need to.
    client_max_body_size 200M;

    error_log /var/log/nginx/rocketchat.access.log;


    location / {
        proxy_pass http://backend/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $http_host;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header X-Nginx-Proxy true;

        proxy_redirect off;
    }
}
EOF
# Spuštění aplikace
systemctl enable --now nginx
```

## Firewall a selinux
Dost pravděpodobně selinux zablokuje přístup na port jiné služby. Pro správné fungování proxy je potřeba to ale povolit. Pokud vás blokuje firewall, otevřete si port 80 (https nepoužíváme).

```
sudo setsebool httpd_can_network_connect 1 -P
firewall-cmd --add-service=http --permanent 
firewall-cmd --add-service --reload
```

## Instalace jinak
Oficiální dokumentace doporučuje instalaci pomocí snap balíčku. Nicméně snap store, ačkoliv je jádro aplikace opensource má některé aspekty, které jsou minimálně problematická (nemožnost provozování vlastních repozitářů, samotný snap store je closed source, apod.). Provozování takové aplikace na druhou stranu je velmi triviální a umožňuje pohodlné aktualizace.

## Závěr
Dnes jsme nainstalovali Rocket.chat jako náhradu slacku, vytvořili pro něj integraci v systemd a připravili proxy. Podívejte se do [https://docs.rocket.chat/](dokumentace), chcete-li vědět více. Vhodné by bylo připravit také SSL certifikát, ale to snad příště. 
